# kops-replication

Replicates critical images for the running of the kubernetes and kubeflow services into `registry.cern.ch`.

Images are replicated using an argo workflow accessible at `https://kops-workflows.cern.ch/workflows` under the namespace of `kops-replication-k8s` for the kubernetes service or `kops-replication-acc` for ATS.

The workflow(s) will fail if the upstream images do not exist; CI verifies this on merge to master. To run locally: `./scripts/validate_images.sh values-k8s.yaml`.
