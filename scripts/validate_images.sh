#!/bin/bash
# validates that newly added images for replication exists upstream.
#
# USAGE: ./scripts/validate_images.sh VALUES_FILE

# Additional arguments to pass to skopeo, skopeo defaults to pulling the system
# architecture which can cause issues if you are on ARM.
SKOPEO_ADDITIONAL_ARGS="${SKOPEO_ADDITIONAL_ARGS:="--raw"}"
# SKOPEO_ADDITIONAL_ARGS="--override-arch amd64 --override-os linux"

# Validate every image in the values file rather that just the newly added ones
# when compared against master.
VALIDATE_ALL_IMAGES=${VALIDATE_ALL_IMAGES:=false}

# ###########################################################################
# generates a list of newly added image artifacts for a repository in a given
# file by comparing its state on the feature branch with that of master.
#
# This assumes we are always merging for master, which should be okay.
#
# INPUTS
# $1 - values.yaml
# $2 - repository
#
# GLOBALS:
# NEW_ARTIFACTS - Contains images to be verified.
# ###########################################################################
generate_image_diff() {
  if [ "$#" -ne 2 ]; then
    echo "illegal number of parameters, excepted 2"
  fi
  VALUES_FILE=$1
  REPOSITORY=$2

  if [[ "$VALIDATE_ALL_IMAGES" = true ]]; then # i.e. do not generate diff.
    NEW_ARTIFACTS=$(cat $VALUES_FILE | yq ".items.\"${REPOSITORY}\".artifacts")
    return
  fi

  git fetch origin master:master
  cat $VALUES_FILE | yq ".items.\"${REPOSITORY}\".artifacts" | sort >/tmp/branch_images.yaml
  git show master:$VALUES_FILE | yq ".items.\"${REPOSITORY}\".artifacts" | sort >/tmp/master_images.yaml

  NEW_ARTIFACTS=$(comm -23 /tmp/branch_images.yaml /tmp/master_images.yaml)
  rm /tmp/branch_images.yaml /tmp/master_images.yaml
}

# ###########################################################################
# private function that verifies an image exists using skopeo, this is called
# by `parallel` to simultaneously check a large number of images. `parallel`
# is used over the &/wait syntax in bash to ensure proper error handling.
#
# INPUTS
# $1 - Artifact whose existence is to be checked.
# ###########################################################################
_check_image_exists() {
  ARTIFACT="$1"
  skopeo inspect $SKOPEO_ADDITIONAL_ARGS "docker://${ARTIFACT}" >/dev/null || {
    echo "image $ARTIFACT does not exist" 1>&2
    exit 1
  }
  echo "image $ARTIFACT exists"
}

# ###########################################################################
# validate_images_exist_upstream is the main entrypoint of this script.
#
# It generates a diff of newly added images (as compared against master) and
# uses skopeo to verify that these images are valid.
#
# INPUTS
# $1 - VALUES_FILE which contains images to be parsed.
# ###########################################################################
validate_images_exist_upstream() {
  if [ "$#" -ne 1 ]; then
    echo "illegal number of parameters, excepted 1"
  fi
  VALUES_FILE=$1

  check_images_exist() {
    export -f _check_image_exists
    export SKOPEO_ADDITIONAL_ARGS

    ARTIFACTS=$@
    echo "${ARTIFACTS[@]}" | tr ' ' '\n' | grep -v '^$' | parallel --will-cite --halt soon,fail=1 _check_image_exists
  }

  IMAGE_REPOSITORIES=$(yq '.items | with_entries(select(.value.type == "oci")) | to_entries | .[].key' $VALUES_FILE)
  for REPOSITORY in ${IMAGE_REPOSITORIES}; do
    echo ">> checking image replication in $REPOSITORY"
    generate_image_diff $VALUES_FILE $REPOSITORY
    check_images_exist $NEW_ARTIFACTS
  done
}

validate_images_exist_upstream "$@"
